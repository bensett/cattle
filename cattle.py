#!/usr/bin/env python3

from collections import OrderedDict
from urllib.parse import quote_plus
from urllib.error import URLError, HTTPError
import warnings
import argparse
import re
import numpy as np
import pandas as pd
from astropy.utils.data import download_file
from astropy.io.votable import parse_single_table
from astropy.utils.exceptions import AstropyWarning
try:
    from tqdm import tqdm
except ModuleNotFoundError:
    def tqdm(iterable):
        return iterable
try:
    from astroquery.simbad import Simbad
    from astroquery.exceptions import TableParseError
    astroquery = True
except ModuleNotFoundError:
    astroquery = False
from astropy import __version__ as apv
av3 = int(apv.split(".")[0]) == 3

def decode(data):
    if av3:
        return data.decode()
    else:
        return data

parser = argparse.ArgumentParser()
parser.add_argument(
    "target_list",
    help="Add comma separated list of targets and calibrators (in square brackets). " +
        "Note the wrapping quote marks are required. Example: " +
        "'IRC +50198, [HR 3662], [SAO 27503], del Leo, [HD 99285]'"
)
parser.add_argument(
    "-o", "--out",
    dest="filename",
    nargs="?",
    const="targets.tex",
    default=False,
    help="Save output to file"
)
parser.add_argument(
    "-b", "--band",
    dest="filters",
    type=str,
    default="VH",
    help="Specify Johnson's magnitudes to use in table as a single word. Default = 'VH'"
)
parser.add_argument(
    "--csv",
    action="store_true",
    help="output as a comma separated value table"
)
args = parser.parse_args()

filters = ["B", "V", "R", "I", "J", "H", "K", "L", "M", "N"]
fmask = [False] * len(filters)
for char in args.filters:
    try:
        fmask[filters.index(char.upper())] = True
    except ValueError:
        continue

target_list = [t.strip() for t in args.target_list.split(",")]

blank_lines = [i for i, t in enumerate(target_list) if t == ""]
target_list = [t for t in target_list if t != ""]

def add_blanks(lst):
    for i in blank_lines:
        lst.insert(i, "")

GetStarURL = "https://apps.jmmc.fr/~sclws/getstar/sclwsGetStarProxy.php?star="

table = OrderedDict([
    ("HD_Number", []),
    ("SpT", []),
    ("ObjCal", [])
])

for flt in filters:
    table[flt] = []

for endmatter in ["AngSize", "RA", "Dec"]:
    table[endmatter] = []

names = []
gsnames = ""
issue = []

def getName(gsTable, names, idx=0):
    if gsTable.array["HD"][idx]:
        return "HD " + decode(gsTable.array["HD"].data[idx])
    target = names[idx]
    if astroquery:
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                simids = [id["ID"] for id in Simbad.query_objectids(target)]
                for identifier in ["HD", "HR", "IRC", "SAO"]:
                    for name in simids:
                        if re.compile(identifier + r".*").search(name) is not None:
                            return name
                return target
            except TableParseError:
                return target
    else:
        return target

def getSize(gsTable, idx=0):
    size = np.nan
    if gsTable.array["diam_count"].data[idx] > 0:
        for diam in ["LDD", "diam_vk", "diam_vj"] + ["UD_"+f for f in "HJKIRVLMNBU"]:
            size = gsTable.array[diam].data[idx]
            if not np.isnan(size):
                break
    return size

def fmtRaDec(RaDec):
    ret = RaDec
    try:
        cutoff = re.compile(r"\.").search(RaDec).span()[1] + 2
        if len(RaDec) > cutoff:
            ret = RaDec[:cutoff]
    finally:
        return ret

for target in target_list:
    iscal = re.compile(r"\[.*\]").search(target)
    if iscal is None:
        table["ObjCal"].append("Obj")
        names.append(target)
    else:
        table["ObjCal"].append("Cal")
        names.append(iscal.group()[1:-1])
    gsnames += quote_plus(names[-1]) + ","

warnings.simplefilter("ignore", category=AstropyWarning)

# Try downloading all of the data at once
try:
    gsTable = parse_single_table(
        download_file(
            GetStarURL + gsnames,
            timeout=30,
            show_progress=False
        )
    )
    if len(gsTable.array) != len(names):
        raise URLError("Could not retrieve all targets")

    table["HD_Number"] = [getName(gsTable, names, i) for i in range(len(gsTable.array))]
    table["SpT"] = [decode(d) for d in gsTable.array["SpType"].data]
    for flt in filters:
        table[flt] = ["{0:.2f}".format(d) for d in gsTable.array[flt].data]
    table["AngSize"] = ["{0:.2f}".format(getSize(gsTable, i)) for i in range(len(gsTable.array))]
    table["RA"] = [fmtRaDec(decode(d)) for d in gsTable.array["RAJ2000"].data]
    table["Dec"] = [fmtRaDec(decode(d)) for d in gsTable.array["DEJ2000"].data]
except URLError:
    def resolve_issue(msg):
        issue.append(msg)
        print(msg)
        for key in table:
            if key == "HD_Number":
                table[key].append(name)
            elif key != "ObjCal":
                table[key].append("?")
    for name in tqdm(names):
        try:
            gsTable = parse_single_table(
                download_file(
                    GetStarURL + quote_plus(name),
                    timeout=30,
                    show_progress=False
                )
            )
        except HTTPError:
            try:
                sim = [id["ID"] for id in Simbad.query_objectids(name)]
                print("% JMMC could not resolve target " + name)
                print("% Found target in Simbad as " + sim[0] + ", trying JMMC again.")
                gsTable = parse_single_table(
                    download_file(
                        GetStarURL + quote_plus(sim[0]),
                        timeout=30,
                        show_progress=False
                    )
                )
            except:
                resolve_issue("% JMMC could not resolve target " + name)
                continue
        except URLError:
            resolve_issue("% Timeout getting data for " + name + " from JMMC.")
            continue

        table["HD_Number"].append(getName(gsTable, [name]))
        table["SpT"].append(decode(gsTable.array["SpType"].data[0]))
        for flt in filters:
            table[flt].append("{0:.2f}".format(gsTable.array[flt].data[0]))
        table["AngSize"].append("{0:.2f}".format(getSize(gsTable)))
        table["RA"].append(fmtRaDec(decode(gsTable.array["RAJ2000"].data[0])))
        table["Dec"].append(fmtRaDec(decode(gsTable.array["DEJ2000"].data[0])))

for k in table.keys():
    add_blanks(table[k])
add_blanks(names)

titles = "\\thead{\\\\ \\textbf{HD Number}} & \\thead{\\\\ \\textbf{SpT}} & "
titles += "\\thead{\\\\ \\textbf{Obj/Cal}}  & "

for (i, flt) in enumerate(filters):
    if fmask[i]:
        titles += "\\thead{\\textbf{" + flt + "} \\\\ \\textbf{(mag)}} & "
    else:
        del table[flt]

titles += "\\thead{\\textbf{Ang Size} \\\\ \\textbf{(mas)}} & \\thead{\\textbf{RA} \\\\ "
titles += "\\textbf{(hr min sec)}} & \\thead{\\textbf{Dec} \\\\ \\textbf{($^{\\circ}$ "
titles += "$^{\\prime}$ $^{\\prime \\prime}$)}} \\\\"

df = pd.DataFrame(table)
if args.csv:
    output = df.to_csv(index=False)
else:
    latex = df.to_latex(index=False).split('\n')[4:-3]
    tab = [latex[i] + " % " + n for (i, n) in enumerate(names)]

    start = [
        "{\\textbf{Object Catalog -- }Please use the magnitude corresponding to telescope " +
        "acquisition and to your selected instrument waveband. (Add rows as needed)}",
        "\\begin{table}[h]",
        "\\centering",
        "\\begin{tabular}{" + ("c" * len(table)) + "}",
        "\\toprule",
        "\\cmidrule(r){1-2}",
        titles,
        "\\midrule"
    ]

    end = ["\\bottomrule", "\\end{tabular}", "\\end{table}\n"]

    output = "\n".join(start + tab + end)
    output += "% Table generated with cattle.py (https://gitlab.chara.gsu.edu/bensett/cattle)\n"

if args.filename is False:
    print("")
    print(output)
else:
    if args.csv and args.filename == "targets.tex":
        args.filename = "targets.csv"
    if issue:
        issue.append("")
    with open(args.filename, "w") as file:
        file.write(output + "\n".join(issue))
