# CATTLE

CATTLE (Chara Automated Target Table for Latex Engine) is a script designed to simplify the
CHARA proposal writing process by generating the Object Catalog for you, provided a list of
targets. Under the hood, this script queries the JMMC
[GetStar](https://www.jmmc.fr/english/tools/proposal-preparation/getstar-61/) service for
stellar properties and [Simbad](http://simbad.u-strasbg.fr/simbad/sim-fid) for name
resolution of each target that does not have an HD number.

## Installation

Copy the `cattle.py` script into your LaTeX project folder or add it to your PATH variable.

Make sure the file is executable with:

```bash
chmod +x cattle.py
```

## Usage

The script expects a comma separated string of targets as input. Targets in square brackets
are denoted as calibrators. Extra commas may be used to add blank lines to the table.

By default, the output is printed to stdout. If you would prefer it printed to file, add a
`-o` after the target list followed by a desired filename (if no filename is provided, the
output will be written to `targets.tex`)

By default, the table will display magnitude columns only for V- and H-bands. If your
acquisition and science wavebands are different, use the `-b` argument followed by the
filters you would like to have displayed.

If you prefer to output to CSV instead of LaTeX, a `--csv` flag is provided.

```bash
./cattle.py 'IRC +50198, [HR 3662], [SAO 27503],, del Leo, [HD 99285]' -o -b VJH
```

## Bugs

Please report bugs to the GitLab issue tracker or email bensett@umich.edu.

## License

[MIT](LICENSE)
